/* SPDX-License-Identifier: GPL-2.0 */
/*
 * Copyright (c) 2022, Ivaylo Ivanov <ivo.ivanov.ivanov1@gmail.com>
 */

#ifndef DEBUG_H_	/* Include guard */
#define DEBUG_H_

#define RED 255,0,0
#define GREEN 0,255,0
#define BLUE 0,0,255
#define WHITE 255,255,255


extern void draw_text(volatile char *fb, const char *text, int textX, int textY, int width, int stride, int r, int g, int b);
long int debug_linecount = 0;

#endif
