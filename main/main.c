/* SPDX-License-Identifier: GPL-2.0 */
/*
 * Copyright (c) 2022, Ivaylo Ivanov <ivo.ivanov.ivanov1@gmail.com>
 */

#include <main.h>

void main(void* dt, void* kernel) {
	/* C entry */
	/* Initialize SoC and Board specific peripherals/quirks */
	soc_init();
	fbprint("soc_init() passed!");
	/* Copy kernel to memory and boot  */
	fbprint("Copying kernel to memory");
	memcpy((void*)CONFIG_PAYLOAD_ENTRY, kernel, (unsigned long) &kernel_size);
	fbprint("Jumping to kernel\nBye bye!");
	load_kernel(dt, 0, 0, 0, (void*)CONFIG_PAYLOAD_ENTRY);	
}